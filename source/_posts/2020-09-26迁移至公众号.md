---
title: 迁移至公众号说明
date: 2020-09-26 09:32:00
tags:
- 杂の文
categories:
- 流萤集
thumbnail: /blog/img/pics/思考者.png
---

## 说明

最新更新烦请在微信公众号中查看：

- 名称：灵感与闪念
- id：cakipaul
- 二维码：

![公众号二维码](/blog/img/pics/公众号二维码.png)

## 2020

- 09.26 [当一切变的复杂，亦本就如此](https://mp.weixin.qq.com/s?__biz=MzU4MzIzMjYyMw==&mid=2247483892&idx=1&sn=f2c2b845c0ac5c8fe1acb33df6398607&chksm=fdad7cf4cadaf5e20bb375c4827b42666ff9ae60db5ba02e277d938ab25ca4f589e7fff036af&token=1546058861&lang=zh_CN#rd)
- 10.02 [创造本身的意义 vs 猴子打字的诡辩](https://mp.weixin.qq.com/s?__biz=MzU4MzIzMjYyMw==&mid=2247483896&idx=1&sn=f34231c4479c3303745eb1988e04a465&chksm=fdad7cf8cadaf5ee0756870536731aa264d6e3f37ba215fbc5f2a6333a47e35c8c6fad653101&token=1546058861&lang=zh_CN#rd)
- 10.03 [浅谈命定论与自由意志](https://mp.weixin.qq.com/s?__biz=MzU4MzIzMjYyMw==&mid=2247483900&idx=1&sn=10833dd8617fa46d27d994bce60022ac&chksm=fdad7cfccadaf5ea8486eb8c751eccff0bbed74412758846d687fb28717d892a17844b4b5bd6&token=1546058861&lang=zh_CN#rd)